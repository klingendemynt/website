from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):
	help = 'Creates a superuser in one command'

	def add_arguments(self, parser):
		parser.add_argument('username', help = "The superuser's username")
		parser.add_argument('email', help = "The superuser's email")
		parser.add_argument('password', help = "The superuser's password")

	def handle(self, *args, **options):
		User.objects.create_superuser(options['username'], options['email'], options['password'])
		self.stdout.write('Superuser created')
