from . import models

# constants

PREFIX_FOR_ACTIVE = 'mannskoret'
PREFIX_FOR_ON_LEAVE = 'prosjekt'
PREFIX_FOR_ALUMN = 'alumni'

PREFIX_FOR_FIRST_TENOR = '1t'
PREFIX_FOR_SECOND_TENOR = '2t'
PREFIX_FOR_FIRST_BASS = '1b'
PREFIX_FOR_SECOND_BASS = '2b'

SPECIAL_FORWARDS = [
	['mannskor', 'mannskoret'],
]

# main functions

def generateEmailForwards(csvWriter):
	writeHeaders(csvWriter)
	writeEmailForwardsForChoirMembers(csvWriter)
	writeEmailForwardsForChoirRoles(csvWriter)
	writeEmailForwardsForChoirGroups(csvWriter)
	writeSpecialEmailForwards(csvWriter)

def writeHeaders(csvWriter):
	csvWriter.writerow(['source', 'target'])

def writeEmailForwardsForChoirMembers(csvWriter):
	for choirMember in models.ChoirMember.objects.all():
		klingendeMyntEmail = klingendeMyntEmailForEmailPrefix(choirMember.user.username)
		csvWriter.writerow([klingendeMyntEmail, str(choirMember.user.email)])
		csvWriter.writerow([klingendeMyntEmailForEmailPrefix(statusPrefixForChoirMember(choirMember)), klingendeMyntEmail])
		if choirMember.status == models.ChoirMember.ACTIVE and choirMember.voice != None:
			csvWriter.writerow([klingendeMyntEmailForEmailPrefix(voicePrefixForChoirMember(choirMember)), klingendeMyntEmail])

def writeEmailForwardsForChoirRoles(csvWriter):
	for choirRole in models.ChoirRole.objects.all():
		if len(choirRole.emailPrefix) > 0:
			klingendeMyntEmail = klingendeMyntEmailForEmailPrefix(choirRole.choirMember.user.username)
			csvWriter.writerow([klingendeMyntEmailForEmailPrefix(choirRole.emailPrefix), klingendeMyntEmail])

def writeEmailForwardsForChoirGroups(csvWriter):
	for choirGroup in models.ChoirGroup.objects.all():
		if len(choirGroup.emailPrefix) > 0:
			for choirRole in choirGroup.choirRoles.all():
				klingendeMyntEmail = klingendeMyntEmailForEmailPrefix(choirRole.choirMember.user.username)
				csvWriter.writerow([klingendeMyntEmailForEmailPrefix(choirGroup.emailPrefix), klingendeMyntEmail])

def writeSpecialEmailForwards(csvWriter):
	for specialForward in SPECIAL_FORWARDS:
		csvWriter.writerow([klingendeMyntEmailForEmailPrefix(specialForward[0]), klingendeMyntEmailForEmailPrefix(specialForward[1])])

# helper functions

def statusPrefixForChoirMember(choirMember):
	if choirMember.status == models.ChoirMember.ACTIVE: return PREFIX_FOR_ACTIVE
	if choirMember.status == models.ChoirMember.ON_LEAVE: return PREFIX_FOR_ON_LEAVE
	if choirMember.status == models.ChoirMember.ALUMN: return PREFIX_FOR_ALUMN
	return None

def voicePrefixForChoirMember(choirMember):
	if choirMember.voice == models.ChoirMember.FIRST_TENOR: return PREFIX_FOR_FIRST_TENOR
	if choirMember.voice == models.ChoirMember.SECOND_TENOR: return PREFIX_FOR_SECOND_TENOR
	if choirMember.voice == models.ChoirMember.FIRST_BASS: return PREFIX_FOR_FIRST_BASS
	if choirMember.voice == models.ChoirMember.SECOND_BASS: return PREFIX_FOR_SECOND_BASS
	return None

def klingendeMyntEmailForEmailPrefix(prefix):
	return prefix + '@klingendemynt.no'
