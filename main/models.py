from django.db.models import *
from django.contrib.auth.models import User

class ChoirMember(Model):
	# any (current or previous) member of the choir
	# always associated with a user (with fields such as username, email, password etc.)
	# the username should be the choir-specific email prefix, the email is the member's personal email
	# this additionally gives every member login capabilities
	# basically, a member must have a status and a voice
	# furthermore, a member can be part of one or more groups, through a role
	
	FIRST_GRADE = 1
	SECOND_GRADE = 2
	THIRD_GRADE = 3
	FOURTH_GRADE = 4
	FIFTH_GRADE = 5
	GRADE_CHOICES = [
		(FIRST_GRADE, 'First grade'),
		(SECOND_GRADE, 'Second grade'),
		(THIRD_GRADE, 'Third grade'),
		(FOURTH_GRADE, 'Fourth grade'),
		(FIFTH_GRADE, 'Fifth grade'),
	]

	INTERN = 'INT'
	ASSOCIATE = 'ASS'
	PARTNER = 'PAR'
	TITLE_CHOICES = [
		(INTERN, 'Intern'),
		(ASSOCIATE, 'Associate'),
		(PARTNER, 'Partner'),
	]

	ACTIVE = 'ACT'
	ON_LEAVE = 'ONL'
	ALUMN = 'ALU'
	STATUS_CHOICES = [
		(ACTIVE, 'Active'),
		(ON_LEAVE, 'On leave'),
		(ALUMN, 'Alumn'),
	]

	FIRST_TENOR = '1T'
	SECOND_TENOR = '2T'
	FIRST_BASS = '1B'
	SECOND_BASS = '2B'
	VOICE_CHOICES = [
		(FIRST_TENOR, 'First tenor'),
		(SECOND_TENOR, 'Second tenor'),
		(FIRST_BASS, 'First bass'),
		(SECOND_BASS, 'Second bass'),
	]

	choirMemberID = AutoField(primary_key = True)
	user = OneToOneField(User, on_delete = CASCADE)
	grade = IntegerField(choices = GRADE_CHOICES, null = True, blank = True)
	title = CharField(max_length = 3, choices = TITLE_CHOICES, blank = True)
	status = CharField(max_length = 3, choices = STATUS_CHOICES)
	voice = CharField(max_length = 2, choices = VOICE_CHOICES, blank = True)
	quote = CharField(max_length = 200, blank = True)
	image = ImageField(upload_to = 'member_images/', blank = True)
	linkedInURL = CharField(max_length = 50, blank = True)
	roles = ManyToManyField('ChoirGroup', through = 'ChoirRole', related_name = 'members')
	
	@property
	def fullName(self):
		return self.user.first_name + ' ' + self.user.last_name

	def __str__(self):
		return str(self.user)

class ChoirRole(Model):
	# a role, associating members with groups in a many-to-many relationship
	# if the role name is blank, the role is regarded 'generic' with respect to the group
	# in that case, the display name and email prefix should also be blank
	# note that a role must not necessarily be associated with a group

	choirRoleID = AutoField(primary_key = True)
	choirMember = ForeignKey('ChoirMember', on_delete = CASCADE, related_name = 'choirRoles')
	choirGroup = ForeignKey('ChoirGroup', on_delete = CASCADE, related_name = 'choirRoles', null = True, blank = True)
	roleName = CharField(blank = True, max_length = 50)
	displayName = CharField(blank = True, max_length = 50)
	emailPrefix = CharField(blank = True, max_length = 50)
	phoneNumber = CharField(blank = True, max_length = 10)
	order = IntegerField(default = 0)

	class Meta:
		ordering = ['-order']

	def getDisplayNameIfDefined(self):
		result = None
		if len(self.displayName) > 0: result = self.displayName
		elif len(self.roleName) > 0: result = self.roleName
		else: result = 'Member'
		return result

	def getDisplayNameIfDefinedIncludingGroup(self):
		result = self.getDisplayNameIfDefined()
		if self.choirGroup != None: result += ', ' + self.choirGroup.getDisplayNameIfDefined()
		return result

	def __str__(self):
		result = self.getDisplayNameIfDefinedIncludingGroup()
		result += ' (' + str(self.choirMember) + ')'
		return result

class ChoirGroup(Model):
	# a group such as 'styret', 'deals' etc.
	# can have super- and sub groups, defining group hierarchies
	# a group is hidden when its existence is not communicated to the public

	choirGroupID = AutoField(primary_key = True)
	superGroup = ForeignKey('self', on_delete = CASCADE, related_name = 'subGroups', null = True, blank = True)
	groupName = CharField(max_length = 50)
	description = CharField(max_length = 1000)
	displayName = CharField(blank = True, max_length = 50)
	hidden = BooleanField(default = False)
	emailPrefix = CharField(blank = True, max_length = 50)
	order = IntegerField(default = 0)
	
	class Meta:
		ordering = ['-order']

	def getDisplayNameIfDefined(self):
		if len(self.displayName) > 0: return self.displayName
		return self.groupName

	def __str__(self):
		return self.getDisplayNameIfDefined()
