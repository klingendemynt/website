from django.forms import ModelForm
from . import models

class ChoirMemberForm(ModelForm):
	class Meta:
		model = models.ChoirMember
		fields = [
			'linkedInURL',
			'quote',
		]

class ChoirMemberImageForm(ModelForm):
	class Meta:
		model = models.ChoirMember
		fields = [
			'image',
		]

class ChoirMemberUserForm(ModelForm):
	class Meta:
		model = models.User
		fields = [
			'first_name',
			'last_name',
			'email',
		]
