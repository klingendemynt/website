from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from . import views

urlpatterns = [
	path('', views.index, name = 'index'),
	path('member/', views.member, name = 'member'),
	path('member/member/', lambda request: redirect('member')), # REQUIRED TO FIX THE IMAGE FORM BUG
	path('member/edit/', views.memberEdit, name = 'member_edit'),
	path('member/edit/image/', views.memberEditImage, name = 'member_edit_image'),
	path('edit/image/', views.memberEditImage), # REQUIRED TO FIX THE IMAGE FORM BUG
	path('member/', include('django.contrib.auth.urls')),
	path('generate_email_forwards/', views.generateEmailForwards, name = 'email_forwards'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
