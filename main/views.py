from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import logout as django_logout
from django.http import HttpResponse
from django.utils import timezone
from django.conf import settings
import csv
from . import emailForwardsGenerator
from . import models
from . import forms

# ----- #
# VIEWS #
# ----- #

def index(request):
	if settings.MAINTENANCE: return render(request, 'main/maintenance.html', {})
	
	activeChoirMembersBasicDescriptions = {}
	for activeChoirMember in getActiveChoirMembers():
		descriptionsList = getSpecialRoleDescriptionsForChoirMember(activeChoirMember)
		descriptionsList.append(activeChoirMember.get_title_display())
		activeChoirMembersBasicDescriptions[activeChoirMember.choirMemberID] = concatStringsNeatly(descriptionsList)

	context = {
		'activeChoirMembers': getActiveChoirMembers(),
		'activeChoirMembersBasicDescriptions': activeChoirMembersBasicDescriptions,
		'choirGroups': getChoirGroups(),
		'voiceDistribution': getVoiceDistribution(),
		'gradeDistribution': getGradeDistribution(),
		'CEORole': getSpecialRoles()['CEO'],
		'showLoginButton': not request.user.is_authenticated
	}

	# renders
	return render(request, 'main/index.html', context)

@login_required(redirect_field_name='')
def member(request):
	loggedInChoirMember = getLoggedInChoirMember(request)
	loggedInChoirMemberRoles = getChoirRolesForChoirMember(loggedInChoirMember)

	context = {
		'loggedInChoirMember': loggedInChoirMember,
		'loggedInChoirMemberRoles': loggedInChoirMemberRoles,
	}

	return render(request, 'main/member.html', context)

@login_required(redirect_field_name='')
def memberEdit(request):
	loggedInChoirMember = getLoggedInChoirMember(request)
	loggedInChoirMemberUserForm = None
	loggedInChoirMemberForm = None

	if request.method == 'POST':
		loggedInChoirMemberUserForm = forms.ChoirMemberUserForm(request.POST, instance = loggedInChoirMember.user)
		loggedInChoirMemberForm = forms.ChoirMemberForm(request.POST, instance = loggedInChoirMember)
		if loggedInChoirMemberUserForm.is_valid() and loggedInChoirMemberForm.is_valid():
			loggedInChoirMemberUserForm.save()
			loggedInChoirMemberForm.save()
			return redirect('member')
	else:
		loggedInChoirMemberUserForm = forms.ChoirMemberUserForm(instance = loggedInChoirMember.user)
		loggedInChoirMemberForm = forms.ChoirMemberForm(instance = loggedInChoirMember)

	context = {
		'loggedInChoirMemberUserForm': loggedInChoirMemberUserForm,
		'loggedInChoirMemberForm': loggedInChoirMemberForm,
	}

	return render(request, 'main/memberEdit.html', context)

@login_required(redirect_field_name='')
def memberEditImage(request):
	loggedInChoirMember = getLoggedInChoirMember(request)
	loggedInChoirMemberImageForm = None

	if request.method == 'POST':
		loggedInChoirMemberImageForm = forms.ChoirMemberImageForm(request.POST, request.FILES, instance = loggedInChoirMember)
		if loggedInChoirMemberImageForm.is_valid():
			loggedInChoirMemberImageForm.save()
			return redirect('member')
	else:
		loggedInChoirMemberImageForm = forms.ChoirMemberImageForm(instance = loggedInChoirMember.user)

	context = {
		'loggedInChoirMemberImageForm': loggedInChoirMemberImageForm,
	}

	return render(request, 'main/memberEditImage.html', context)

@staff_member_required(redirect_field_name='')
def generateEmailForwards(request):
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="forwards.csv"'
	writer = csv.writer(response)
	emailForwardsGenerator.generateEmailForwards(writer)	
	return response

def error(request, exception = None):
	return render(request, 'main/error.html', {})

# ------- #
# HELPERS #
# ------- #

def getActiveChoirMembers():
	return models.ChoirMember.objects.filter(status = models.ChoirMember.ACTIVE).order_by('user__last_name')

def getLoggedInChoirMember(request):
	return models.ChoirMember.objects.filter(user = request.user).get()

def getChoirGroups():
	return models.ChoirGroup.objects.filter(hidden = False)

def getChoirRolesForChoirMember(choirMember):
	return models.ChoirRole.objects.filter(choirMember = choirMember)

def getSpecialRoles():
	specialRoles = {}
	for specialRole in models.ChoirRole.objects.filter(roleName__in = ['CEO', 'CFO', 'CMO', 'MC']):
		specialRoles[specialRole.roleName] = specialRole
	return specialRoles

def getSpecialRoleDescriptionsForChoirMember(choirMember):
	specialRoleDescriptions = []
	for (specialRoleName, specialRole) in getSpecialRoles().items():
		if specialRole.choirMember == choirMember:
			specialRoleDescriptions.append(specialRoleName)
	return specialRoleDescriptions

def getVoiceDistribution():
	voiceDistribution = []
	for (voice, voiceDescription) in models.ChoirMember.VOICE_CHOICES:
		voiceDistribution.append(getActiveChoirMembers().filter(voice = voice).count())
	return voiceDistribution

def getGradeDistribution():
	gradeDistribution = []
	for (grade, gradeDescription) in models.ChoirMember.GRADE_CHOICES:
		gradeDistribution.append(getActiveChoirMembers().filter(grade = grade).count())
	return gradeDistribution

def concatStringsNeatly(strings):
	result = strings[0]
	if len(strings) == 1:
		return result
	for i in range(1, len(strings) - 1):
		result += ', ' + strings[i]
	result += ' & ' + strings[-1]
	return result
