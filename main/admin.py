from django.contrib import admin
from . import models

class ChoirMemberAdmin(admin.ModelAdmin):
	exclude = [
		'image',
	] # REQUIRED TO FIX THE IMAGE FORM BUG

admin.site.register(models.ChoirMember, ChoirMemberAdmin)
admin.site.register(models.ChoirRole)
admin.site.register(models.ChoirGroup)
