# Generated by Django 2.2.3 on 2019-08-07 15:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_choirmember_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choirrole',
            name='choirGroup',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='choirRoles', to='main.ChoirGroup'),
        ),
        migrations.AlterField(
            model_name='choirrole',
            name='choirMember',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='choirRoles', to='main.ChoirMember'),
        ),
    ]
