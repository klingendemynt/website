# Generated by Django 2.2.3 on 2019-08-10 09:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_auto_20190807_1726'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='choirgroup',
            options={'ordering': ['-order']},
        ),
        migrations.AlterModelOptions(
            name='choirrole',
            options={'ordering': ['-order']},
        ),
        migrations.AlterField(
            model_name='choirgroup',
            name='description',
            field=models.CharField(max_length=1000),
        ),
    ]
