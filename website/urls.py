from django.contrib import admin
from django.urls import include, path

urlpatterns = [
	path('', include('main.urls')),
	path('songs/', include('songs.urls')),
	path('admin/', admin.site.urls),
]

handler400 = 'main.views.error'
handler403 = 'main.views.error'
handler404 = 'main.views.error'
handler500 = 'main.views.error'
