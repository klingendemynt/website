from django.db.models import *
from django.core.validators import FileExtensionValidator

class Song(Model):
	songID = AutoField(primary_key = True)
	name = CharField(max_length = 50)
	abbreviation = CharField(max_length = 10, blank = True)
	demo = FileField(upload_to = 'songs/', validators = [FileExtensionValidator(['mp3'])], null = True)
	categories = ManyToManyField('SongCategory', related_name = 'songs')

	class Meta:
		ordering = ['name']

	def __str__(self):
		return str(self.name)

class SongCategory(Model):
	songCategoryID = AutoField(primary_key = True)
	name = CharField(max_length = 50)
	abbreviation = CharField(max_length = 10, blank = True)

	class Meta:
		ordering = ['name']
		verbose_name_plural = 'song categories'

	def __str__(self):
		return str(self.name)
