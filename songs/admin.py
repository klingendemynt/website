from django.contrib import admin
from . import models

class SongAdmin(admin.ModelAdmin):
	filter_horizontal = [
		'categories',
	]

admin.site.register(models.Song, SongAdmin)
admin.site.register(models.SongCategory)
