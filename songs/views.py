from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from . import models

@login_required(redirect_field_name='')
def allSongs(request):
	allSongs = models.Song.objects.all()

	context = {
		'heading': 'All songs',
		'songs': allSongs,
	}

	return render(request, 'songs/songs.html', context)

@login_required(redirect_field_name='')
def searchedSongs(request, abbreviation):
	matchingSongCategories = models.SongCategory.objects.filter(abbreviation = abbreviation)
	matchingSongs = models.Song.objects.filter(abbreviation = abbreviation)

	songs = []
	if matchingSongCategories.count() == 1: songs = matchingSongCategories.get().songs.all()
	elif matchingSongs.count() == 1: songs = matchingSongs

	context = {
		'heading': 'Songs matching ' + abbreviation,
		'songs': songs,
	}

	return render(request, 'songs/songs.html', context)
