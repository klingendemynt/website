from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

urlpatterns = [
	path('', views.allSongs, name = 'all_songs'),
	path('<slug:abbreviation>/', views.searchedSongs, name = 'searched_songs'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
